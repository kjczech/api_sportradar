""" SportRadar_MLB_v7 api getter functions """

from contextlib import contextmanager
#import awswrangler.secretsmanager as sm
import requests
import pytest
#import datetime
from pathlib import Path
import time
from urllib.parse import urlparse
import pathlib
import yaml
import xml
import json
import os
import numpy as np

_MAX_RETRIES = 20
_CURRENT_RETRIES = 0
_RETRY_WAIT_TIME = 5.0

def pathposix(path):
    outpath = pathlib.PureWindowsPath(path)
    return outpath.as_posix()

def mk_json(input_dict, path, name, indent_spaces=4, mode='w'):
    with open('{}/{}.json'.format(path, name), mode) as f:
        json.dump(input_dict, f, indent=indent_spaces,sort_keys=True, default=str)

# functional approach to pulling the api credentials
def get_aws_secret_name():
    return "sportradar_api"

# define get_api_key and get_base_url functions for all API packages
# get_api_key
def get_api_key():
    #aws_secret_name = get_aws_secret_name()
    #api_key = sm.get_secret_json(aws_secret_name).get("api_key")
    api_key = 'ncaup7gtmsxf9snzfhx9eap8'
    
    if not api_key:
        raise ConnectionError(
            """Unable to obtain api_key from secret manager.
            Check that you have configured your aws connection via
            aws-cli or aws-shell and that you have obtained the appropriate
            credentials (i.e. Administrator_accessKeys from within the
            target aws account.""")
                        
    return api_key




# get_base_url for league_hierarchy
def get_league_hierarchy_endpoint() -> str:
    #aws_secret_name = get_aws_secret_name()
    #base_url = sm.get_secret_json(aws_secret_name).get("league_hierarchy_url")
    base_url = 'https://api.sportradar.us/mlb/trial/v7/en/league/hierarchy.json'
    output = "{base_url}?api_key={api_key}".format(base_url=base_url, api_key=get_api_key())
    return output

# get league_hierarchy json object from request
def get_league_hierarchy(max_retries: int = _MAX_RETRIES,
                         try_number = _CURRENT_RETRIES) -> dict:
    endpoint = get_league_hierarchy_endpoint()
    #return get_api_request_robust(endpoint=endpoint).json()
    #return requests.get(url=endpoint).json()
    
    while try_number < max_retries:
        try:
            output = requests.get(url=endpoint).json()
            break
        except json.JSONDecodeError:
            try_number += 1
            time.sleep(_RETRY_WAIT_TIME)
            get_league_hierarchy(try_number=try_number)
    if output:
        return output
    else:
        print('failed')
    

# get_base_url for league_venues
def get_league_venues_endpoint() -> str:
    #aws_secret_name = get_aws_secret_name()
    #base_url = sm.get_secret_json(aws_secret_name).get("league_venues_url")
    base_url = 'https://api.sportradar.us/mlb/trial/v7/en/league/venues.json'
    output = "{base_url}?api_key={api_key}".format(base_url=base_url, api_key=get_api_key())
    return output

# get league_venues json object from request
#def get_league_venues() -> dict:
#    endpoint = get_league_venues_endpoint()
#    #return requests.get(url=endpoint).json()
#    return get_api_request_robust(endpoint=endpoint).json()

# get league_venues json object from request
def get_league_venues(max_retries: int = _MAX_RETRIES,
                      try_number = _CURRENT_RETRIES) -> dict:
    endpoint = get_league_venues_endpoint()
    #return requests.get(url=endpoint).json()
    #return get_api_request_robust(endpoint=endpoint).json()
    ###output = get_api_request_robust(endpoint=endpoint).json()
    while try_number < max_retries:
        try:
            output = requests.get(url=endpoint).json()
            break
        except json.JSONDecodeError:
            try_number += 1
            time.sleep(_RETRY_WAIT_TIME)
            get_league_venues(try_number=try_number)
    if output:
        return output
    else:
        print('failed')
        

def extract_league_hierarchy_response_data(input_dict : dict) -> dict:
    output_idx = 1
    output_dict = {}
    for i in input_dict['leagues']:
        
        for j in i['divisions']:
            for k in j['teams']:
                output_dict[output_idx] = {
                    
                    'league_id' : i['id'],
                    'league_name' : i['name'],
                    'division_id' : j['id'],
                    'division_name' : j['name'],
                    'team_id' : k['venue']['id'],
                    'team_name' : k['name'],
                    'team_market' : k['market'],
                    'team_name_abbreviation' : k['abbr'],
                    'venue_id' : k['venue']['id'],
                    'venue_name' : k['venue']['name'],
                    'venue_market' : k['venue']['market'],
                    'venue_address' : k['venue']['address'],
                    'venue_city' : k['venue']['city'],
                    'venue_state' : k['venue']['state'],
                    'venue_zip' : k['venue']['zip'],
                    'venue_country' : k['venue']['country'],
                    'venue_latitude' : k['venue']['location']['lat'],
                    'venue_longitude' : k['venue']['location']['lng'],
                    'venue_orientation' : k['venue']['field_orientation'], # direction pitcher throws ball away from and direction catcher faces while catching
                    'venue_type' :  k['venue']['stadium_type'],
                    'venue_playing_surface' :  k['venue']['surface'],
                }
                output_idx += 1
    return output_dict


def get_list_of_league_hierarchy_output_keys(hierarchy_output_dict : dict) -> list:
    headers_list = []
    for item in list(hierarchy_output_dict[1].keys()):
        headers_list.append(item)
    return headers_list

def get_string_of_league_hierarchy_output_keys(hierarchy_output_dict : dict) -> str:
    headers_list = get_list_of_league_hierarchy_output_keys(hierarchy_output_dict)
    headers_string = ""
    for item in headers_list:
        headers_string += "{},".format(item)
    return headers_string.rsplit(',', maxsplit=1)[0]


def append_id_column_to_string(id_column_name : str,
                               headers_string : str,
                               append_where='head') -> str:
    if append_where == 'head':
        return '{id_column_name}'.format(id_column_name=id_column_name) + ',' + headers_string
    elif append_where == 'tail':
        return  headers_string + ',' +  '{id_column_name}'.format(id_column_name=id_column_name)
    else:
        raise KeyError('{} is not a supported `append_where` input value'.format(append_where))


def get_league_hierarchy_csv_headers(hierarchy_output_dict : dict) -> str:
    hierarchy_header_string = get_string_of_league_hierarchy_output_keys(hierarchy_output_dict)
    headers_string = append_id_column_to_string('hierarchy_id', headers_string= hierarchy_header_string)
    return headers_string
    


def flat_dict_to_list_of_values_rows(hierarchy_output_dict : dict) -> list:
    output_rows = []
    dict_length = len(hierarchy_output_dict)
    headers_list = get_list_of_league_hierarchy_output_keys(hierarchy_output_dict)
    for i in range(1, dict_length + 1):
        lines = '{}'.format(i)
        for header in headers_list:
            lines += ",{}".format(hierarchy_output_dict[i][header])
        output_rows.append(lines)
    return output_rows
    
def make_league_hierarchy_csv(output_dir : str, filename : str):
    try:
        hierarchy_dict = get_league_hierarchy()
    except json.JSONDecodeError:
        hierarchy_dict = get_league_hierarchy()
    #hierarchy_dict = get_league_hierarchy()
    hierarchy_output_dict = extract_league_hierarchy_response_data(hierarchy_dict)
    headers_string = get_league_hierarchy_csv_headers(hierarchy_output_dict)
    hierarchy_output_rows = flat_dict_to_list_of_values_rows(hierarchy_output_dict)
    
    with open(output_dir + '\\' + filename + '.csv', 'w') as f:
        f.write(headers_string + '\n')
        
        for row in hierarchy_output_rows:
                f.write(row + '\n')