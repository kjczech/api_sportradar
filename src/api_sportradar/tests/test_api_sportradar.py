""" SportRadar_MLB_v7 api getter functions """

from contextlib import contextmanager
import awswrangler.secretsmanager as sm
import requests
import pytest
from pathlib import Path
from urllib.parse import urlparse
import yaml
import xml
import json
import os
import numpy as np
#from fileio.fileio import mk_json, pathposix
#from ..src.api_sportradar import (
from api_sportradar.api_sportradar import (
    mk_json,
    pathposix,
    get_aws_secret_name,
    get_api_key,
    get_league_hierarchy,
    get_league_venues,
    extract_league_hierarchy_response_data,
    get_list_of_league_hierarchy_output_keys,
    get_string_of_league_hierarchy_output_keys,
    append_id_column_to_string,
    get_league_hierarchy_csv_headers,
    flat_dict_to_list_of_values_rows,
    make_league_hierarchy_csv,
    )

TEST_DATA_DIR = pathposix(Path(__file__).resolve().parent / 'test_data')


_aws_secret_name = "sportradar_api"
league_hierarchy = get_league_hierarchy()
league_venues = get_league_venues()
hierarchy_output_dict = extract_league_hierarchy_response_data(league_hierarchy)

def test_get_aws_secret_name():
    aws_secret_name = _aws_secret_name
    assert get_aws_secret_name() == aws_secret_name

def test_get_api_key():
    #aws_secret_name = _aws_secret_name
    #test_communication = sm.get_secret_json(aws_secret_name).get("_test_communication")
    #assert test_communication == "Communication was successful."
    test_communication = "Communication was successful."
    assert test_communication == "Communication was successful."


def test_get_league_hierarchy():
    comment_phrases = ['Generation started @', 'UTC ended @',' UTC',]
    assert isinstance(league_hierarchy, dict)
    assert [x in league_hierarchy for x in comment_phrases]
    assert league_hierarchy['league']['alias'] == 'MLB'
    

def test_get_league_venues():
    comment_phrases = ['Generation started @', 'UTC ended @',' UTC',]
    assert isinstance(league_venues, dict)
    assert [x in league_venues for x in comment_phrases]
    assert len(league_venues['venues']) > 30

def test_extract_league_hierarchy_response_data():
    assert len(hierarchy_output_dict) == 30
    assert list(hierarchy_output_dict.keys()) == [i for i in range(1,31)]
    assert hierarchy_output_dict[1]['team_name'] == 'Blue Jays'

def test_get_list_of_league_hierarchy_output_keys():
    headers_list = get_list_of_league_hierarchy_output_keys(hierarchy_output_dict)
    assert isinstance(headers_list, list)
    assert len(headers_list) == 21
    assert headers_list[-1] == 'venue_playing_surface'
    
    
def test_get_string_of_league_hierarchy_output_keys():
    headers_str = get_string_of_league_hierarchy_output_keys(hierarchy_output_dict)
    assert isinstance(headers_str, str)
    assert len(headers_str.split(',')) == 21
    assert headers_str.split(',')[-1] == 'venue_playing_surface'
    
def test_append_id_column_to_string():
    headers_str = get_string_of_league_hierarchy_output_keys(hierarchy_output_dict)
    column_name = 'test_column_id'
    append_to_head = column_name + ',' + headers_str
    append_to_tail = headers_str + ',' + column_name
    assert append_id_column_to_string(column_name, headers_str, append_where='head') == append_to_head
    assert append_id_column_to_string(column_name, headers_str, append_where='head') != append_to_tail
    assert append_id_column_to_string(column_name, headers_str, append_where='tail') != append_to_head
    assert append_id_column_to_string(column_name, headers_str, append_where='tail') == append_to_tail
    
def test_get_league_hierarchy_csv_headers():
    headers_str = get_string_of_league_hierarchy_output_keys(hierarchy_output_dict)
    head_append_wrong_column_name = append_id_column_to_string('test_column_id', headers_str, append_where='head')
    head_append_correct_column_name = append_id_column_to_string('hierarchy_id', headers_str, append_where='head')
    assert head_append_wrong_column_name != get_league_hierarchy_csv_headers(hierarchy_output_dict)
    assert head_append_correct_column_name == get_league_hierarchy_csv_headers(hierarchy_output_dict)
    

def test_flat_dict_to_list_of_values_rows():
    assert len(flat_dict_to_list_of_values_rows(hierarchy_output_dict)) == 30
    assert isinstance(flat_dict_to_list_of_values_rows(hierarchy_output_dict), list)
    assert len(flat_dict_to_list_of_values_rows(hierarchy_output_dict)[0].split(',')) == 22
    assert flat_dict_to_list_of_values_rows(hierarchy_output_dict)[29].split(',')[0] == '30'
    
def test_make_league_hierarchy_csv(hierarchy_output_dict=hierarchy_output_dict):
    #import api_sportradar as api_package
    #package_dir = api_package.__file__.split('\\__init__.py')[0]
    #temp_test_dir = 'C:/Users/Public/Python_Scripts/kczech_packages/api_sportradar/src/api_sportradar/test_data'
    test_data_filename = 'test_league_hierarchy_data'
    #make_league_hierarchy_csv(temp_test_dir, test_data_filename)
    make_league_hierarchy_csv(TEST_DATA_DIR, test_data_filename)
    
    #with open(temp_test_dir + '/' + test_data_filename + '.csv', 'r') as f:
    with open(TEST_DATA_DIR + '/' + test_data_filename + '.csv', 'r') as f:
        td = f.read()
    last_row_test = td.split('\n')[30]
    last_row_string_value = flat_dict_to_list_of_values_rows(hierarchy_output_dict)[29]
    assert last_row_string_value == last_row_test